# Docker settings for Dedoles site

- default root for web is `/var/www`
- to access dedoles-web container bash run `sh bash-server.sh` 
- the source files for web are defined in `docker-compose.yml:15` such as `../dedoles`

- Web with ssl (apache2) listen to port 8082
- MySQL (mariadb) listen to port 3340
- Solr listen to port 8984
- Beanstalk port is listening on 11301
- Redis - without opened port
- Varnish - without opened port

To connect to container's bash, use the `bash bash-server.sh` script.

## Running
1) pull git to the folder, where the dedoles root exists.
it has to looks like:
`path/dedoles`
`path/dedoles-docker`
the `path/dedoles` contains dedoles project files (to change the path to the project files, change the `docker-compose.yml:13`)
2) fetch git submodules if they are not fetched already
3) `./run.sh` run containers needed for application (`docker-compose up -d` runs on background)
4) add `dedoles.docker` and all other sites to the hosts file (list of sites can be found in any apache config)
5) project web is available at the address `dedoles.docker` to prevent conflicts with apache2
6) add permissions for dedolesdbuser to mysql `GRANT ALL PRIVILEGES ON *.* TO 'dedolesdbuser'@'%' IDENTIFIED BY 'dedovlese';`
7) import init data to the database (docker has to be running)
```bash
mysql -udedolesdbuser -p buxus_dedoles_dev -P3340 -h dedoles.docker < images/db/structure.sql
mysql -udedolesdbuser -p buxus_dedoles_dev -P3340 -h dedoles.docker < images/db/trigger1.sql
mysql -udedolesdbuser -p buxus_dedoles_dev -P3340 -h dedoles.docker < images/db/trigger2.sql
mysql -udedolesdbuser -p buxus_dedoles_dev -P3340 -h dedoles.docker < mDumpData.sql
```
8) `.env` file settings are available in the dedoles repository, so pleace link `.env.local` to `.env` in the dedoles folder


## Local configuration
To run dedoles among local apache2 instance, use the config that can be found in `./images/webserver/0dedoles.conf`

