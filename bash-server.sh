#!/usr/bin/env bash

users=("dedoles" "root" "other (If You want to user custom username choose this)")

echo $'\e[32;1mPlease choose predefined username by writing a number and hitting [ENTER]\e[0m'
select username in "${users[@]}"
do
    if [[ $username == "${users[2]}" ]]; then
        echo -n "Please enter username and then press [ENTER]:"
        read username
    fi

    if [ -z "${username}" ]; then
        echo -n "Username cannot be empty!"
        echo
        exit
    fi
    break;
done

echo
echo $'\e[32;1mPlease choose container to connecto to and hit [ENTER]\e[0m'

docker_containers=(`docker ps --format "{{.Names}}"`)

select container in "${docker_containers[@]}"
do
    if [ ${container} == 'dedoles-web' ]; then
        script="sh -c \"cd /home/dedoles/live/current && bash\""
    else
        script=bash
    fi

    echo
    eval "docker exec -u ${username} -it ${container} ${script}"
    \

    break
done
